# skill/r-properties-demo

Demonstrates how properties may be used in skill using the R programming language.

## Properties definition

What (and how many) properties are defined is at the skill developers discretion. Properties are defined within the
`skill.yml definition`.

This demo can be configured using the `MANIPULATION_METHOD` property which is defined as following:
```
properties:
  - name: "MANIPULATION_METHOD"
    desc: "Configures how the skill manipulates requests"
    default: "UPPER"
    pattern: "(UPPER|LOWER)"
```

If the property is omitted it will default to `UPPER` as defined. Using `pattern` allows the skill developer to
restrict the property values to the defined regex pattern. In this case the value can only be `UPPER` or `LOWER`.

## Properties access

Properties can be accessed directly from the environment:
```
evaluate <- function(payload, context) {
    manipulation_method <- Sys.getenv(x = "MANIPULATION_METHOD")
    ...
}
```
